﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using CKWIS.Support.Helpers;
using RestDemo.Models;
using RestDemo.Helpers;
using System.Net.Http;
using System.Data.SqlTypes;

namespace RestDemo
{
    class Program
    {
        private static string sVIN;
        private static string sMake;
        private static string sPartNo;
        private static string sPartDesc;
        private static string sPartOrder;
        private static int iClaimID;
        private static int lMakeID;
        private static int lQty;
        private static int lClaimDetailID;
        private static int iCKID;
        private static string sCON;

        public static double CastDouble(string value, double defaultValue)
        {
            double item = 0;
            if (double.TryParse(value, out item))
            {
                return item;

            }
            return defaultValue;
        }

        static void Main(string[] args)
        {            
            string[] ar = args;
            lClaimDetailID = int.Parse(args[0]);
            //iClaimID = Convert.ToInt32(args[4]);
            sCON = "server=142.11.233.98;database=veritas;User Id=sa;Password=NCC1701E";
            setICKID();
            string SQL;
            SQL = "select * from claimdetail where claimdetailid = " + Convert.ToString(lClaimDetailID);
            DBO.clsDBO clR = new DBO.clsDBO();
            clR.OpenDB(SQL, sCON);
            if (clR.RowCount > 0)
            {
                clR.GetRow();
                sPartNo = clR.get_Fields("partno");
                SQL = "select * from claim where claimid = " + clR.get_Fields("claimid");
                clR.OpenDB(SQL, sCON);
                if (clR.RowCount > 0)
                {
                    clR.GetRow();
                    SQL = "select * from contract where contractid = " + clR.get_Fields("contractid");
                    clR.OpenDB(SQL, sCON);
                    if (clR.RowCount > 0)
                    {
                        clR.GetRow();
                        sVIN = clR.get_Fields("vin");
                        sMake = clR.get_Fields("make");
                    }
                }
            }

            var username = ConfigurationManager.AppSettings["UserName"];
            var pwd = ConfigurationManager.AppSettings["Password"];
            var grantType = ConfigurationManager.AppSettings["grant_type"];
            var tokenRoute = ConfigurationManager.AppSettings["TokenRoute"];
            var baseEndpoint = ConfigurationManager.AppSettings["BaseEndPoint"];
            var quote = ConfigurationManager.AppSettings["Quote"];
            var makes = ConfigurationManager.AppSettings["Makes"];
            var tokenTimeout = TimeSpan.FromSeconds(CastDouble(ConfigurationManager.AppSettings["TokenRequestTimeout"], 3));
            var basicTimeout = TimeSpan.FromSeconds(CastDouble(ConfigurationManager.AppSettings["RequestTimeout"], 60));
            lQty = 1;
            var restClient = GetRestHelper();
            var config = new CKWebapiHTTPTokenProviderConfig
            {
                BaseAddress = new Uri(baseEndpoint),
                TokenReq = new TokenRequest(username, pwd, grantType),
                TokenRoute = tokenRoute,
                TokenTimeout = tokenTimeout

            };

            var tokenProvider = GetTokenProvider(config, restClient);
            var hack1 = GetMakes(restClient, tokenProvider, basicTimeout, baseEndpoint, makes);

            hack1.Wait();
            var makesResult = hack1.Result;

            foreach (var results in makesResult)
            {
                if (results.make.ToLower() == sMake.ToLower().Trim())
                {
                    lMakeID = results.oemid;
                    goto MoveHere;
                }
                else
                {
                    lMakeID = 0;
                }
            }
            
MoveHere:           
            
            var hack2 = GetQuote(restClient, tokenProvider, basicTimeout, baseEndpoint, quote, sVIN);
            hack2.Wait();
            var quotesResult = hack2.Result;            
            var O = quotesResult.oem;
            if (O.Count>0)
            {
                var oem = quotesResult.oem[0];
                var Warranty1 = oem.warranty[0];
                var Warranty2 = oem.warranty[1];
                sPartDesc = oem.description;
                SQL = "update claimdetail set claimdesc = '" + sPartDesc + "' where claimdetailid = " + Convert.ToString(lClaimDetailID);
                clR.RunSQL(SQL, sCON);
            }
        }

        public static async Task<List<MakeItem>> GetMakes(IRestHelper restHelper, ITokenProvider tokenProvider, TimeSpan timeOut, string baseUrl, string route)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(baseUrl);
                httpClient.Timeout = timeOut;

                return await restHelper.ServiceRequestNoValidation<List<MakeItem>>(httpClient, HttpMethod.Get, route, new Dictionary<string, string>(), tokenProvider);

            }
        }


        public static async Task<QuoteResponse> GetQuote(IRestHelper restHelper, ITokenProvider tokenProvider, TimeSpan timeOut, string baseUrl, string route, string VIN)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(baseUrl);
                httpClient.Timeout = timeOut;
                var partsList = new List<Part>();
                partsList.Add(new Part { Partnumber = sPartNo, Quantity = Convert.ToInt32(1), Showaftermarketoption = false });
                var request = new QuoteRequest
                {
                    oemid = lMakeID,
                    vin = sVIN,
                    parts = partsList
                };
                return await restHelper.ServiceRequestObject<QuoteRequest, QuoteResponse>(httpClient, HttpMethod.Post, route,request, tokenProvider);

            }
        }

        public static IRestHelper GetRestHelper()
        {
            
            return new RestHelper();
        }

        public static ITokenProvider GetTokenProvider(CKWebapiHTTPTokenProviderConfig config, IRestHelper restHelper)
        {
            return new CKWebapiHTTPTokenProvider(config, restHelper);
        }

        public static void setICKID()
        {
            DBO.clsDBO cCKID = new DBO.clsDBO();                       
            string sSQL = "select * from ckQuote where ClaimID = " + iClaimID;
            cCKID.OpenDB(sSQL, sCON);
            if (cCKID.RowCount > 0)
            {
                cCKID.GetRow();
                iCKID = Convert.ToInt32(cCKID.get_Fields("CKID"));
            }
            
        }

        public static async Task<OrderResponse> PlaceOrder(IRestHelper restHelper, ITokenProvider tokenProvider, TimeSpan timeout, string baseUrl, string route, OrderRequestPart[] partRequests)
        {


            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(baseUrl);
                httpClient.Timeout = timeout;

                DBO.clsDBO clR = new DBO.clsDBO();
                DBO.clsDBO clCL = new DBO.clsDBO();
                DBO.clsDBO clU = new DBO.clsDBO();
                DBO.clsDBO clC = new DBO.clsDBO();
                DBO.clsDBO clSC = new DBO.clsDBO();
                string SQL;
                string sClaimID = "";
                string sContractID = "";
                string sServiceCenterID = "";
                string sUserName = "";
                string sUserEMail = "";
                string sContractNo = "";
                string sAuthorizationNumber = "";
                string sRepairOrderNumber = "";
                string sOwner = "";
                string sVin = "";
                string sMileage = "";
                string sRepairSite = "";
                string sAddress = "";
                string sPostalCode = "";
                string sPhone = "";
                string sContact = "";
                string sWarranty = "";
                string sEocMileage = "";
                string sEocDate = "";
                string sNote = "";



                SQL = "select * from claimdetail " +
                    "where claimdetailid = " + lClaimDetailID;
                clR.OpenDB(SQL, sCON);
                if (clR.RowCount > 0)
                {
                    clR.GetRow();
                    sClaimID = clR.get_Fields("ClaimID");
                }

                SQL = "select * from claim where claimid = " + sClaimID;
                clCL.OpenDB(SQL, sCON);
                if (clCL.RowCount > 0)
                {
                    clCL.GetRow();
                    sContractID = clCL.get_Fields("ContractID");
                    sRepairOrderNumber = clCL.get_Fields("RONumber");
                    sAuthorizationNumber = clCL.get_Fields("ClaimNo");
                    sMileage = clCL.get_Fields("lossMile");
                    sServiceCenterID = clCL.get_Fields("servicecenterID");
                    try
                    {
                        sContact = clCL.get_Fields("SCContactInfo");
                    }
                    catch
                    {
                    }

                }

                SQL = "select * from contract where contractid = " + sContractID;
                clC.OpenDB(SQL, sCON);
                if (clC.RowCount > 0)
                {
                    clC.GetRow();
                    sContractNo = clC.get_Fields("ContractNo");
                    sOwner = clC.get_Fields("fname") + " " + clC.get_Fields("lname");
                    sVin = clC.get_Fields("Vin");
                }

                SQL = "select * from servicecenter where servicecenterID = " + sServiceCenterID;
                clSC.OpenDB(SQL, sCON);
                if (clSC.RowCount > 0)
                {
                    clSC.GetRow();
                    sRepairSite = clSC.get_Fields("ServiceCenterName");
                    sAddress = clSC.get_Fields("Addr1");
                    try
                    {
                        sAddress = sAddress + " " + clSC.get_Fields("Addr2");
                    }
                    catch
                    {

                        throw;
                    }//Addr2
                    sPostalCode = clSC.get_Fields("Zip");
                    sPhone = clSC.get_Fields("Phone");
                }




                var orderRequest = new OrderRequest
                {

                    adjuster = sUserName,
                    adjusteremail = sUserEMail,
                    contractnumber = sContractNo,
                    authorizationnumber = sAuthorizationNumber,
                    repairordernumber = sRepairOrderNumber,
                    owner = sOwner,
                    vin = sVin,
                    mileage = sMileage,
                    repairsite = sRepairSite,
                    address = sAddress,
                    postalcode = sPostalCode,
                    phone = sPhone,
                    contact = sContact,
                    warranty = sWarranty,
                    eocmileage = sEocMileage,
                    eocdate = sEocDate,
                    note = sNote,
                    parts = partRequests
                };
                var response = await restHelper.ServiceRequestObjectNoValidation<OrderRequest, OrderResponse>(httpClient, HttpMethod.Post, route, orderRequest, tokenProvider);

                return response;
            }

        }
    }
}
