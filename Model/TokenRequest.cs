﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestDemo.Models
{
    class TokenRequest
    {
        public TokenRequest()
        { }
        public TokenRequest(string username, string password, string grantType) {
            Username = username;
            Password = password;
            Grant_type = grantType;
        }

        public string Username { get; private set; }
        public string Password { get; private set; }
        public string Grant_type { get; private set; }
    }
}
