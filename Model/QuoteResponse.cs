﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace RestDemo.Models
{
    class QuoteResponse : CKWIS.Support.Models.ISelfValidate
    { /// <summary>
      /// Results list of OEM part(s) searched
      /// </summary>
        public List<OEMPartResponse> oem { get; set; }
        /// <summary>
        /// Results list of Aftermarket part(s) searched
        /// </summary>
        public List<AftermarketPartResponse> aftermarket { get; set; }
        /// <summary>
        /// Error message if any
        /// </summary>
        public string error { get; set; }
        public bool ErrorEncountered { get; private set; }

        public Exception Ex { get; private set; }

        public HttpStatusCode ResponseCode { get; private set; }

        public void SelfValidate(bool error, Exception ex, HttpStatusCode responseCode)
        {
            ErrorEncountered = error;
            Ex = ex;
            ResponseCode = responseCode;
            if (error)
            {
                oem = new List<OEMPartResponse>();
                aftermarket = new List<AftermarketPartResponse>();
            }

        }
    }
    

}
